package uk.co.zanaku.asteroids;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by Tom on 06/12/2014.
 */
public class Bullet implements Phased{

    public float timer = 0;
    public float lifeTime = 1;
    public boolean dead = false;

    public BodyDef bodyDef;
    public Body body;
    public CircleShape shape;
    public FixtureDef fixtureDef;
    public Fixture fixture;

    public Texture texture;
    public Sprite sprite;

    public Phase phase;

    public Bullet(World world, AssetManager assets, Phase phase, float angle, float impulse, float x, float y){
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x,y);
        bodyDef.bullet = true;

        body = world.createBody(bodyDef);
        shape = new CircleShape();
        shape.setRadius(0.5f);

        fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 50f;
        fixtureDef.isSensor = false;

        fixture = body.createFixture(fixtureDef);

        texture = assets.get("bullet.png");
        texture.setFilter(Constants.filter,Constants.filter);

        sprite = new Sprite(texture);
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);

        body.applyLinearImpulse(MathUtils.cos(angle)*impulse,MathUtils.sin(angle)*impulse,0,0,true);
        body.setUserData(this);

        this.phase = phase;

        }

    public void update(float deltaTime){
        timer+=deltaTime;
        if(timer>lifeTime){
            dead=true;
        }
    }

    public void redraw(){
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);
    }

    public void render(SpriteBatch batch,Phase worldPhase){
        Color tint = this.phase.getTint();
        if(!phase.interactable(worldPhase, true)) {
            tint.a=0.5f;
        }
        sprite.setColor(tint);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setPosition(body.getPosition().x*Constants.BOX_TO_WORLD-sprite.getWidth()/2,body.getPosition().y*Constants.BOX_TO_WORLD-sprite.getHeight()/2);
        sprite.draw(batch);
    }

    public boolean isDead(){
        return dead;
    }

    public void dispose(){
        shape.dispose();
    }

    @Override
    public Phase getPhase() {
        return phase;
    }

    @Override
    public void setPhase(Phase phase) {
        this.phase = phase;
    }
}
