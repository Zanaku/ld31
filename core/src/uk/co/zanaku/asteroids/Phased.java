package uk.co.zanaku.asteroids;

/**
 * Created by Tom on 07/12/2014.
 */
public interface Phased {
    public Phase getPhase();
    public void setPhase(Phase phase);
}
