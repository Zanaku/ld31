package uk.co.zanaku.asteroids;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Tom on 06/12/2014.
 */
public class Player implements Phased{

    public float rotation = 10f;

    public int bulletMax = 10;
    public float bulletDelay = 0;
    public float shotSpeed = 0.4f;
    public float bulletImpulse = 3000.0f;

    private float forwardAcceleration = 280.0f;
    private float reverseAcceleration = forwardAcceleration/2;

    public BodyDef bodyDef;
    public Body body;
    public PolygonShape shape;
    public FixtureDef fixtureDef;
    public Fixture fixture;

    public Texture texture;

    public Sprite sprite;

    public boolean dead;

    public Phase phase;

    public boolean invincible = false;

    public Player(World world, AssetManager assets, Phase phase, float x, float y){
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x,y);
        bodyDef.angularDamping = 5f;
        bodyDef.linearDamping = 1f;

        body = world.createBody(bodyDef);

        shape = new PolygonShape();
        shape.setAsBox(3.0f,2.0f);

        fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0.5f;
        fixtureDef.restitution = 2f;

        fixture = body.createFixture(fixtureDef);

        texture = assets.get("ship.png");
        texture.setFilter(Constants.filter,Constants.filter);

        sprite = new Sprite(texture);
        sprite.setSize(3*2*Constants.BOX_TO_WORLD,2*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);

        body.setUserData(this);

        bulletDelay = 0;

        this.phase = phase;
    }

    public void redraw(){
        sprite.setSize(3*2*Constants.BOX_TO_WORLD,2*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);
    }

    public void render(SpriteBatch batch,Phase worldPhase){
/*        Color tint = this.phase.getTint();
        if(!phase.interactable(worldPhase)) {
            tint.a=0.5f;
        }
        sprite.setColor(tint);*/
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setPosition(body.getPosition().x*Constants.BOX_TO_WORLD-sprite.getWidth()/2,body.getPosition().y*Constants.BOX_TO_WORLD-sprite.getHeight()/2);
        sprite.draw(batch);
    }

    public Vector2 forwardAcceleration(){
        float angleRadians = this.body.getAngle();
        float xAcc = MathUtils.cos(angleRadians)*forwardAcceleration;
        float yAcc = MathUtils.sin(angleRadians)*forwardAcceleration;
        return new Vector2(xAcc,yAcc);
    }

    public Vector2 reverseAcceleration(){
        Vector2 forward = this.forwardAcceleration();
        return new Vector2(-forward.x/2,-forward.y/2);
    }

    public void fireWeapon(World world,AssetManager assets,Array<Bullet> bullets){
        if(bullets.size>=this.bulletMax){return;}
        Bullet bullet = new Bullet(world,assets, this.phase, this.body.getAngle(),this.bulletImpulse,this.body.getPosition().x,this.body.getPosition().y);
        bullets.add(bullet);
    }

    public void dispose(){
        shape.dispose();
    }

    @Override
    public Phase getPhase() {
        return phase;
    }

    @Override
    public void setPhase(Phase phase) {
        this.phase = phase;
    }
}
