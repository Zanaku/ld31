package uk.co.zanaku.asteroids;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Tom on 06/12/2014.
 */
public class Constants {

    public static int WINDOW_WIDTH;
    public static int WINDOW_HEIGHT;
    public static float BOX_TO_WORLD = 5f;
    public static float WORLD_TO_BOX = 1/BOX_TO_WORLD;
    public static Texture.TextureFilter filter = Texture.TextureFilter.Linear;

    public static enum PHASE{RED(0),YELLOW(1),GREEN(2),CYAN(3),BLUE(4),MAGENTA(5),WHITE(6);
        private int value;
        private PHASE(int value){
            this.value = value;
        }
    };



}
