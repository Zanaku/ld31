package uk.co.zanaku.asteroids;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Iterator;

public class Asteroids extends ApplicationAdapter {

    Preferences prefs;

    int gameState;
    int priorGameState;
    static final int RUNNING = 0;
    static final int PAUSED = 1;

    int mode;
    static final int MENU = 0;
    static final int SCORE_ATTACK = 1;

    OrthographicCamera camera;
    Viewport viewport;

    AssetManager assets;

    Box2DDebugRenderer debugRenderer;
    World world;
    SpriteBatch batch;
    BitmapFont font;

    Texture background;

    Player player;

    Array<Bullet> bullets;
    Iterator<Bullet> bulletIterator;

    Array<Asteroid> asteroids;

    Array<MenuAsteroid> menuAsteroids;

    Array<Body> asteroidsForRemoval;
    Array<Body> bulletsForRemoval;
    Array<Body> menusForRemoval;

    Array<Body> bodyList;

    public boolean debug = false;

    int score;
    int lives;

    boolean initialised = false;
    boolean modeInitialised = false;

    boolean won;
    boolean died;

    int stage;
    int highscore;
    float timeSinceLastSpawn;

    Sprite indicatorRed;
    Sprite indicatorGreen;
    Sprite indicatorBlue;

    Phase worldPhase;

	@Override
	public void create () {



        generateWindowSizeConstants();

        camera = new OrthographicCamera();
        camera.setToOrtho(false,Constants.WINDOW_WIDTH,Constants.WINDOW_HEIGHT);
        camera.position.set((Constants.WINDOW_WIDTH/2),(Constants.WINDOW_HEIGHT/2),0);
        camera.update();
        viewport = new ExtendViewport(800,600);

        batch = new SpriteBatch();
        font = new BitmapFont();


        prefs = Gdx.app.getPreferences("LudumoidsPreferences.xml");
        highscore = prefs.getInteger("HighScore");

        loadAssets();

        gameState=0;
        mode=0;

        debugRenderer = new Box2DDebugRenderer();

        asteroidsForRemoval = new Array<Body>();
        bulletsForRemoval = new Array<Body>();
        menusForRemoval = new Array<Body>();
        bodyList = new Array<Body>();

	}

    public void loadAssets() {
        assets = new AssetManager();
        assets.load("gutterImage.png", Texture.class);
        assets.load("background.png", Texture.class);
        assets.load("background_old2.png", Texture.class);
        assets.load("background_old.png", Texture.class);
        assets.load("ship.png",Texture.class);
        assets.load("shipLarge.png",Texture.class);
        assets.load("asteroidLarge.png",Texture.class);
        assets.load("asteroidMedium.png",Texture.class);
        assets.load("asteroidSmall.png",Texture.class);
        assets.load("colourIndicator.png",Texture.class);
        assets.load("menuAsteroidLargeScoreAttack.png",Texture.class);
        assets.load("menuAsteroidLargeFullscreen.png",Texture.class);
        assets.load("menuAsteroidLarge1920x1080.png",Texture.class);
        assets.load("menuAsteroidLarge1280x720.png",Texture.class);
        assets.load("menuAsteroidLarge1024x576.png",Texture.class);
        assets.load("menuAsteroidLargeQuit.png",Texture.class);
        assets.load("InstructionText.png",Texture.class);
        assets.load("bullet.png",Texture.class);
        assets.load("asteroidExplode.wav", Sound.class);
        assets.load("shoot.wav", Sound.class);
        assets.load("playerHit.wav",Sound.class);
        assets.load("phase.wav",Sound.class);
        assets.load("playerExplosion.wav",Sound.class);
        assets.load("title.png",Texture.class);
        assets.load("oneUp.wav",Sound.class);
        assets.load("oneDown.wav",Sound.class);
        assets.load("playerInvincibilityOn.wav",Sound.class);
    }

    public void resizeWindow(int width,int height,boolean fullscreen){
        Gdx.graphics.setDisplayMode(width,height,fullscreen);
        viewport.update(width,height);
        generateScaleConstants();
        redrawSprites();
        prefs.putInteger("width",width);
        prefs.putInteger("height",height);
        prefs.putBoolean("fullscreen",Gdx.graphics.isFullscreen());
        prefs.flush();
    }

    public void generateWindowSizeConstants(){
        Constants.WINDOW_WIDTH = Gdx.graphics.getWidth();
        Constants.WINDOW_HEIGHT = Gdx.graphics.getHeight();

        System.err.println("WINDOW_WIDTH:"+Constants.WINDOW_WIDTH+" WINDOW_HEIGHT: "+Constants.WINDOW_HEIGHT+" BOX_TO_WORLD: "+Constants.BOX_TO_WORLD+" WORLD_TO_BOX: "+Constants.WORLD_TO_BOX);
    }
    public void generateScaleConstants(){
        float ratio = (float)viewport.getWorldWidth()/(float)viewport.getWorldHeight();
        if(ratio>1.4){
            Constants.BOX_TO_WORLD = 6f;
            Constants.WORLD_TO_BOX = 1 / Constants.BOX_TO_WORLD;
        }
        else{
            Constants.BOX_TO_WORLD = 5f;
            Constants.WORLD_TO_BOX = 1 / Constants.BOX_TO_WORLD;
        }
    }

    public void redrawSprites(){
        if(player!=null)
            player.redraw();
        if(asteroids!=null){
            for(Asteroid asteroid:asteroids){
            asteroid.redraw();
            }
        }
        if(bullets!=null) {
            for (Bullet bullet : bullets) {
                bullet.redraw();
            }
        }
        if(menuAsteroids!=null) {
            for (MenuAsteroid menu : menuAsteroids) {
                menu.redraw();
            }
        }
    }

    public void init(){

        resizeWindow(prefs.getInteger("width",1280),prefs.getInteger("height",720),prefs.getBoolean("fullscreen",false));

        world = new World(new Vector2(0,0),true);

        worldPhase = new Phase(true,true,true);

        player = new Player(world,assets, worldPhase, (Constants.WINDOW_WIDTH/2)*Constants.WORLD_TO_BOX,(Constants.WINDOW_HEIGHT/2)*Constants.WORLD_TO_BOX);

        stage=0;
        timeSinceLastSpawn=0;

        score = 0;
        lives = 3;
        won=false;
        died=false;

        bullets = new Array<Bullet>();
        asteroids = new Array<Asteroid>();
        menuAsteroids = new Array<MenuAsteroid>();

        indicatorRed = new Sprite((Texture)assets.get("colourIndicator.png"));
        indicatorRed.getTexture().setFilter(Constants.filter,Constants.filter);
        indicatorRed.setPosition(Constants.WINDOW_WIDTH-80,20);
        indicatorGreen = new Sprite((Texture)assets.get("colourIndicator.png"));
        indicatorGreen.getTexture().setFilter(Constants.filter,Constants.filter);
        indicatorGreen.setPosition(Constants.WINDOW_WIDTH-65,45);
        indicatorBlue = new Sprite((Texture)assets.get("colourIndicator.png"));
        indicatorBlue.getTexture().setFilter(Constants.filter,Constants.filter);
        indicatorBlue.setPosition(Constants.WINDOW_WIDTH-50,20);

        createCollisionListener();
        initialised=true;
    }

    public void initScoreAttack(){
        world.getBodies(bodyList);
        for(Body body: bodyList){
            if(body.getUserData() instanceof MenuAsteroid) {
                world.destroyBody(body);
            }
        }
        modeInitialised=true;

    };

    public void initMenu(){
        MenuAsteroid scoreAttackAsteroid = new MenuAsteroid(world,assets,"menuAsteroidLargeScoreAttack.png",MenuAsteroid.START_SCORE_ATTACK,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+30,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX);
        MenuAsteroid quitAsteroid = new MenuAsteroid(world,assets,"menuAsteroidLargeQuit.png",MenuAsteroid.QUIT,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+30,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX-30);
        MenuAsteroid asteroid1024576 = new MenuAsteroid(world,assets,"menuAsteroidLarge1024x576.png",MenuAsteroid.RESOLUTION_1024x576,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+65,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX+45);
        MenuAsteroid asteroid720 = new MenuAsteroid(world,assets,"menuAsteroidLarge1280x720.png",MenuAsteroid.RESOLUTION_1280x720,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+65,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX+15);
        MenuAsteroid asteroid1080 = new MenuAsteroid(world,assets,"menuAsteroidLarge1920x1080.png",MenuAsteroid.RESOLUTION_1920x1080,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+65,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX-15);
        MenuAsteroid fullscreenAsteroid = new MenuAsteroid(world,assets,"menuAsteroidLargeFullscreen.png",MenuAsteroid.FULLSCREEN,this,worldPhase,Constants.WINDOW_WIDTH/2* Constants.WORLD_TO_BOX+65,Constants.WINDOW_HEIGHT/2* Constants.WORLD_TO_BOX-45);
        menuAsteroids.add(scoreAttackAsteroid);
        menuAsteroids.add(quitAsteroid);
        menuAsteroids.add(fullscreenAsteroid);
        menuAsteroids.add(asteroid1024576);
        menuAsteroids.add(asteroid720);
        menuAsteroids.add(asteroid1080);
        modeInitialised=true;
    }

	@Override
	public void render () {
        float dT = Gdx.graphics.getDeltaTime();
        if(assets.update()){

            update(dT);
            Gdx.gl.glClearColor(1,1,1,1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            Matrix4 camCopy = camera.combined.cpy();
            Color batchColor = new Color(batch.getColor());
            Color worldColor = worldPhase.getTint();
            batch.begin();
            if(mode!=MENU) {
                background = assets.get("background.png");
                if(worldColor.r<1||worldColor.g<1||worldColor.b<1){
                    worldColor.a = 0.8f;
                }
                batch.setColor(worldColor);
                background.setFilter(Constants.filter, Constants.filter);
                batch.draw(background, 0, 0, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
            }
            else{
                background = assets.get("background_old2.png");
                background.setFilter(Constants.filter, Constants.filter);
                worldColor.a = 0.5f;
                batch.setColor(worldColor);
                batch.draw(background, 0, 0, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
                Texture instText =assets.get("InstructionText.png");
                instText.setFilter(Constants.filter,Constants.filter);
                batch.draw(instText,20,80,400,400);
                Texture title =assets.get("title.png");
                instText.setFilter(Constants.filter,Constants.filter);
                batch.draw(title,Constants.WINDOW_WIDTH/2-300,Constants.WINDOW_HEIGHT-180);
            }
            batch.setColor(batchColor);

            for(Asteroid asteroid:asteroids){
                asteroid.render(batch,worldPhase);
            }
            for(Bullet bullet:bullets){
                bullet.render(batch,worldPhase);
            }

            if(!died) {
                player.render(batch,worldPhase);
            }
            if(mode==SCORE_ATTACK){
                font.draw(batch,"LIVES: "+lives+" Score: "+score+" HighScore: "+highscore+" Stage: "+stage,30,30);
                if(won){
                    font.draw(batch,"You Won! Press R to play again, or M to return to the Menu.",Constants.WINDOW_WIDTH/2-120,Constants.WINDOW_HEIGHT/2);
                }
                if(died){
                    font.draw(batch,"You Died!",Constants.WINDOW_WIDTH/2-30,Constants.WINDOW_HEIGHT/2+15);
                    font.draw(batch,"Press R to play again.",Constants.WINDOW_WIDTH/2-70,Constants.WINDOW_HEIGHT/2);
                    font.draw(batch,"Press M to return to the Menu.",Constants.WINDOW_WIDTH/2-90,Constants.WINDOW_HEIGHT/2-15);
                }
            }
            if(mode==MENU){
                for(MenuAsteroid menuAsteroid: menuAsteroids){
                    menuAsteroid.render(batch,worldPhase);
                }

                font.draw(batch,"HighScore: "+highscore,30,30);

            }
            if(gameState==PAUSED){
                font.draw(batch,"PAUSED.",Constants.WINDOW_WIDTH/2-45,Constants.WINDOW_HEIGHT/2);
            }
            if(worldPhase.red){
                indicatorRed.setColor(1,0,0,1);
            }
            else{
                indicatorRed.setColor(1,1,1,1);
            }
            indicatorRed.draw(batch);
            if(worldPhase.green){
                indicatorGreen.setColor(0,1,0,1);
            }
            else{
                indicatorGreen.setColor(1,1,1,1);
            }
            indicatorGreen.draw(batch);
            if(worldPhase.blue){
                indicatorBlue.setColor(0,0,1,1);
            }
            else{
                indicatorBlue.setColor(1,1,1,1);
            }
            indicatorBlue.draw(batch);
            if(debug){
                font.draw(batch,"DEBUG",Constants.WINDOW_WIDTH-60,Constants.WINDOW_HEIGHT-10);
            }
            batch.end();

            if(debug){
                debugRenderer.render(world, camCopy.scl(Constants.BOX_TO_WORLD));
            }

        }
        else{
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.begin();
            font.draw(batch, "Loading Assets, Please Hold. " + (int) (assets.getProgress() * 100) + "%", Constants.WINDOW_WIDTH / 2 - 90, Constants.WINDOW_HEIGHT / 2);
            batch.end();
        }

	}

    public void update(float dT){
        if(!initialised){
            init();
        }
        if(gameState==RUNNING) {
            if(mode==SCORE_ATTACK){
                if(!modeInitialised){
                    initScoreAttack();

                }
                calculateStage();
                runStage(stage, dT);
                processAsteroidsAndBulletsForRemoval();
                checkLoseState();

                if (score > highscore) {
                    highscore = score;
                }
            }
            if(mode==MENU){
                if(!modeInitialised){
                    if(world.getBodyCount()>0){
                        resetWorld();
                        init();
                    }
                    initMenu();
                }
                processMenusAndBulletsForRemoval();
            }
            if(debug){
                processDebugInput();
            }
            processMovementInput();
            processWeaponInput(dT);
            processPhaseInput();
            checkForDeadBullets(dT);

            world.step(1 / 60f, 6, 2);
            updateBodyPositions();
        }
        processUtilityInput();
    }

    public void updateBodyPositions(){
        world.getBodies(bodyList);
        for(Body body:bodyList){
            if(body.getPosition().x*Constants.BOX_TO_WORLD>Constants.WINDOW_WIDTH+5){
                body.setTransform(new Vector2(0, body.getPosition().y), body.getAngle());
            }
            if(body.getPosition().x*Constants.BOX_TO_WORLD<-5){
                body.setTransform(new Vector2(Constants.WINDOW_WIDTH*Constants.WORLD_TO_BOX,body.getPosition().y),body.getAngle());
            }
            if(body.getPosition().y*Constants.BOX_TO_WORLD>Constants.WINDOW_HEIGHT+5){
                body.setTransform(new Vector2(body.getPosition().x, 0), body.getAngle());
            }
            if(body.getPosition().y*Constants.BOX_TO_WORLD<-5){
                body.setTransform(new Vector2(body.getPosition().x,Constants.WINDOW_HEIGHT*Constants.WORLD_TO_BOX),body.getAngle());
            }
        }
    }

    public void checkWinState(){
        if(asteroids.size==0){
            won=true;
        }
        if(won){
            if(Gdx.input.isKeyJustPressed(Keys.SPACE)){
                resetWorld();
                init();
            }
        }
    }

    public void checkLoseState(){
        if(lives<1 && !died){
            died=true;
            world.destroyBody(player.body);
            player.dispose();
            ((Sound)assets.get("playerExplosion.wav")).play();
            prefs.putInteger("HighScore",highscore);
            prefs.flush();
        }
    }

    public void resetWorld(){
        world.getBodies(bodyList);
        for(Body body: bodyList){
            world.destroyBody(body);
        }
    }

    public void spawnAsteroids(int required,Phase phase){
        float minDist = (Constants.WINDOW_WIDTH*Constants.WORLD_TO_BOX*Constants.WINDOW_HEIGHT*Constants.WORLD_TO_BOX/480);
        float radians, dx, dy;
        for(int i = 0;i<required;i++){
            radians = MathUtils.radiansToDegrees*MathUtils.random(0,360);
            dx = MathUtils.cos(radians) * 5f;
            dy = MathUtils.sin(radians) * 5f;

            float x = MathUtils.random(Constants.WINDOW_WIDTH*Constants.WORLD_TO_BOX);
            float y = MathUtils.random(Constants.WINDOW_HEIGHT*Constants.WORLD_TO_BOX);
            float distX = x - player.body.getPosition().x;
            float distY = y - player.body.getPosition().y;
            float dist = (float)Math.sqrt(distX*distX+distY*distY);
            while(dist<minDist){
                x = MathUtils.random(Constants.WINDOW_WIDTH*Constants.WORLD_TO_BOX);
                y = MathUtils.random(Constants.WINDOW_HEIGHT*Constants.WORLD_TO_BOX);
                distX = x - player.body.getPosition().x;
                distY = y - player.body.getPosition().y;
                dist = (float)Math.sqrt(distX*distX+distY*distY);
                System.err.println("Figuring out Asteroid #"+i+" Trying: "+"x: "+x+" y: "+y+" distX: "+distX+" distY:"+distY+" dist:"+dist+" minDist:"+minDist);
            }
            Asteroid asteroid = new Asteroid(world, assets, phase, Asteroid.LARGE, x,y);
            asteroid.body.setLinearVelocity(dx,dy);
            asteroids.add(asteroid);
        }
    }

    public void resize(int width,int height){
        viewport.update(width,height);
        generateScaleConstants();
        redrawSprites();
    }

    public void drawGutters(){
        Texture gutterImage = assets.get("gutterImage.png");
        FitViewport fitViewport = (FitViewport)viewport;
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();
        Gdx.gl.glViewport(0, 0, screenWidth, screenHeight);
        batch.getProjectionMatrix().idt().setToOrtho2D(0, 0, screenWidth, screenHeight);
        batch.getTransformMatrix().idt();
        batch.begin();
        float leftGutterWidth = viewport.getLeftGutterWidth();
        if(leftGutterWidth>0){
            batch.draw(gutterImage,0,0,leftGutterWidth,screenHeight);
            batch.draw(gutterImage,fitViewport.getRightGutterX(),0,fitViewport.getRightGutterWidth(),screenHeight);
        }
        batch.end();
        viewport.update(screenWidth, screenHeight, true);
    }

    public void changeMode(int mode){
        this.mode = mode;
        modeInitialised=false;
    }

    public void processAsteroidsAndBulletsForRemoval(){
        Iterator<Body> iterator = asteroidsForRemoval.iterator();
        while(iterator.hasNext()){
            Body body = iterator.next();
            ((Asteroid)body.getUserData()).shatter(world,assets, asteroids);
            ((Sound)assets.get("asteroidExplode.wav")).play();
            world.destroyBody(body);
        }
        iterator = bulletsForRemoval.iterator();
        while(iterator.hasNext()){
            Body body = iterator.next();
            world.destroyBody(body);
        }
        asteroidsForRemoval.clear();
        bulletsForRemoval.clear();
    }

    public void processMenusAndBulletsForRemoval(){
        Iterator<Body> iterator = menusForRemoval.iterator();
        while(iterator.hasNext()){
            Body body = iterator.next();
            ((MenuAsteroid)body.getUserData()).doTask();
            ((Sound)assets.get("asteroidExplode.wav")).play();
            world.destroyBody(body);
        }
        iterator = bulletsForRemoval.iterator();
        while(iterator.hasNext()){
            Body body = iterator.next();
            world.destroyBody(body);
        }
        asteroidsForRemoval.clear();
        bulletsForRemoval.clear();
        menusForRemoval.clear();
    }


    public void processMovementInput(){
        if(Gdx.input.isKeyPressed(Keys.UP) || Gdx.input.isKeyPressed(Keys.W)) {
            player.body.applyForce(player.forwardAcceleration(),player.body.getPosition(),true);
        }
        else if(Gdx.input.isKeyPressed(Keys.DOWN) || Gdx.input.isKeyPressed(Keys.S)) {
            player.body.applyForce(player.reverseAcceleration(),player.body.getPosition(),true);
        }
        if(Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
            player.body.applyAngularImpulse(player.rotation, true);
        }
        else if(Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
            player.body.applyAngularImpulse(-player.rotation,true);
        }


    }

    public void processPhaseInput(){
        if(Gdx.input.isKeyJustPressed(Keys.J) || Gdx.input.isKeyJustPressed(Keys.Z)) {
            if(worldPhase.blue||worldPhase.green){
                worldPhase.red = !worldPhase.red;
                ((Sound)assets.get("phase.wav")).play();
            }
            System.err.println("PHASE RED! "+worldPhase.red);
        }
        else if(Gdx.input.isKeyJustPressed(Keys.K) || Gdx.input.isKeyJustPressed(Keys.X)) {
            if(worldPhase.red||worldPhase.blue){
                worldPhase.green = !worldPhase.green;
                ((Sound)assets.get("phase.wav")).play();
            }
            System.err.println("PHASE GREEN! "+worldPhase.green);

        }
        else if(Gdx.input.isKeyJustPressed(Keys.L) || Gdx.input.isKeyJustPressed(Keys.C)) {
            if(worldPhase.red||worldPhase.green){
                worldPhase.blue = !worldPhase.blue;
                ((Sound)assets.get("phase.wav")).play();
            }
            System.err.println("PHASE BLUE! "+worldPhase.blue);
        }
    }

    public void processUtilityInput(){
        if(Gdx.input.isKeyJustPressed(Keys.F8)){
            debug =!debug;
        }
        if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
            Gdx.app.exit();
        }
        if(Gdx.input.isKeyJustPressed(Keys.P)){
            if(gameState!=PAUSED) {
                priorGameState=gameState;
                gameState = PAUSED;
            }
            else{
                gameState = priorGameState;
            }
        }
        if(Gdx.input.isKeyJustPressed(Keys.R)){
            resetWorld();
            init();
        }
        if(Gdx.input.isKeyJustPressed(Keys.M)){
            changeMode(0);
        }
    }

    public void processDebugInput(){
        if(mode!=MENU){
            if(Gdx.input.isKeyJustPressed(Keys.F9)){
                lives--;
                ((Sound)assets.get("oneDown.wav")).play();
            }
            if(Gdx.input.isKeyJustPressed(Keys.F10)){
                lives++;
                ((Sound)assets.get("oneUp.wav")).play();
            }
            if(Gdx.input.isKeyJustPressed(Keys.F5)){
                score-=100;
                ((Sound)assets.get("oneDown.wav")).play();
            }
            if(Gdx.input.isKeyJustPressed(Keys.F6)){
                score+=100;
                ((Sound)assets.get("oneUp.wav")).play();
            }
            if(Gdx.input.isKeyJustPressed(Keys.F7)){
                score+=6000;
                ((Sound)assets.get("oneUp.wav")).play();
            }
            if(Gdx.input.isKeyJustPressed(Keys.F11)){
                player.invincible=!player.invincible;
                ((Sound)assets.get("playerInvincibilityOn.wav")).play();
            }
        }

    }

    public void checkForDeadBullets(float deltaTime){
        bulletIterator = bullets.iterator();
        while(bulletIterator.hasNext()){
            Bullet bullet = bulletIterator.next();
            bullet.update(deltaTime);
            if(bullet.isDead()){
                bullet.dispose();
                world.destroyBody(bullet.body);
                bulletIterator.remove();
            }
        }
    }

    public void processWeaponInput(float deltaTime){
        if(player.bulletDelay>0){
            player.bulletDelay-=deltaTime;
        }
        if(Gdx.input.isKeyPressed(Keys.SPACE)){
            if(player.bulletDelay<=0){
                player.fireWeapon(world,assets,bullets);
                ((Sound)assets.get("shoot.wav")).play();
                player.bulletDelay+=player.shotSpeed;
            }
        }
    }


    public void dispose(){
        assets.dispose();
    }


    public void collideAsteroidAndBullet(Body asteroid, Body bullet) {
        earnPoints(((Asteroid) asteroid.getUserData()).type);
        Asteroid asteroidObject = ((Asteroid) asteroid.getUserData());
        asteroidObject.linearVelocityAtTimeOfDeath = asteroidObject.body.getLinearVelocity();
        Bullet bulletObject = ((Bullet) bullet.getUserData());
        asteroids.removeValue(asteroidObject, true);
        bullets.removeValue(bulletObject, true);
        if (!asteroidsForRemoval.contains(asteroid, true)){
            asteroidsForRemoval.add(asteroid);
        }
        if(!bulletsForRemoval.contains(bullet,true)) {
            bulletsForRemoval.add(bullet);
        }
    }

    public void collideMenu(Body menu,Body bullet){
        MenuAsteroid menuObj = ((MenuAsteroid)menu.getUserData());
        Bullet bulletObject = ((Bullet) bullet.getUserData());
        //menuAsteroids.removeValue(menuObj,true);
        bullets.removeValue(bulletObject, true);
/*        if (!menusForRemoval.contains(menu, true)){
            menusForRemoval.add(menu);
        }*/
        ((MenuAsteroid)menu.getUserData()).doTask();
        ((Sound)assets.get("asteroidExplode.wav")).play();
        if(!bulletsForRemoval.contains(bullet,true)) {
            bulletsForRemoval.add(bullet);
        }

    }

    public void earnPoints(int type) {
        int bonus = worldPhase.activeColours();
        score+=bonus*10;
        if(type<2){
            score+=bonus*5;
        }
        if(type<1){
            score+=bonus*5;
        }
    }

    public void calculateStage(){
        if(stage<12){
            stage = score/500;
        }
        if(stage>12){
            stage = 12;
        }
    }

    public void runStage(int stage,float dT){
        timeSinceLastSpawn+=dT;
        int colour;
        if(stage==0) {
            if(asteroids.size<1||timeSinceLastSpawn>10) {
                colour = MathUtils.random(4);
                if(colour!=0&&colour!=2&&colour!=4){
                    colour-=1;
                }
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==1) {
            if(timeSinceLastSpawn>9) {
                colour = MathUtils.random(4);
                if(colour!=0&&colour!=2&&colour!=4){
                    colour-=1;
                }
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==2) {
            if(timeSinceLastSpawn>8) {
                colour = MathUtils.random(4);
                if(colour!=0&&colour!=2&&colour!=4){
                    colour-=1;
                }
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==3) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(4);
                if(colour!=0&&colour!=2&&colour!=4){
                    colour-=1;
                }
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==4) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(5);
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==5) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==6) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(2, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==7) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==8) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                colour = MathUtils.random(6);
                spawnAsteroids(1, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==9) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                colour = MathUtils.random(6);
                spawnAsteroids(2, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==10) {
            if(timeSinceLastSpawn>7) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==11) {
            if(timeSinceLastSpawn>6) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }
        if(stage==12) {
            if(timeSinceLastSpawn>5) {
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                colour = MathUtils.random(6);
                spawnAsteroids(3, new Phase(Constants.PHASE.values()[colour]));
                timeSinceLastSpawn=0;
            }
        }

    }

    public void createCollisionListener(){
        world.setContactListener(new ContactListener(){

            @Override
            public void beginContact(Contact contact) {
            }

            @Override
            public void endContact(Contact contact) {
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
                Body bodyA = contact.getFixtureA().getBody();
                Body bodyB = contact.getFixtureB().getBody();

                if(mode==MENU){
                    if(bodyA.getUserData() instanceof MenuAsteroid && bodyB.getUserData() instanceof Bullet){
                        collideMenu(bodyA,bodyB);
                    }
                    if(bodyB.getUserData() instanceof MenuAsteroid && bodyA.getUserData() instanceof Bullet){
                        collideMenu(bodyB,bodyA);
                    }
                }

                if(bodyA.getUserData() instanceof Player && bodyB.getUserData() instanceof Bullet){
                    contact.setEnabled(false);
                }
                if(bodyB.getUserData() instanceof Player && bodyA.getUserData() instanceof Bullet){
                    contact.setEnabled(false);
                }
                if(!((Phased)bodyA.getUserData()).getPhase().interactable(((Phased)bodyB.getUserData()).getPhase(),worldPhase)){
                    contact.setEnabled(false);
                }
                else{
                    if(bodyA.getUserData() instanceof Asteroid && bodyB.getUserData() instanceof Player){
                        if(!player.invincible){
                            lives--;
                        }
                        ((Sound)assets.get("playerHit.wav")).play();
                    }
                    else if(bodyB.getUserData() instanceof Asteroid && bodyA.getUserData() instanceof Player){
                        if(!player.invincible){
                            lives--;
                        }
                        ((Sound)assets.get("playerHit.wav")).play();
                    }
                    if(bodyA.getUserData() instanceof Asteroid && bodyB.getUserData() instanceof Bullet){
                        collideAsteroidAndBullet(bodyA,bodyB);
                    }
                    else if(bodyB.getUserData() instanceof Asteroid && bodyA.getUserData() instanceof Bullet){
                        collideAsteroidAndBullet(bodyB,bodyA);
                    }
                }

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });
    }


}
