package uk.co.zanaku.asteroids;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Tom on 07/12/2014.
 */
public class Phase {
    public boolean red;
    public boolean green;
    public boolean blue;

    public Phase(boolean red, boolean green, boolean blue){
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Phase(Phase phase){
        this.red = phase.red;
        this.green = phase.green;
        this.blue = phase.blue;
    }

    public Phase(Constants.PHASE phase){
        switch(phase){
            case RED:     this.red=true;
                          this.green=false;
                          this.blue=false;
                          break;
            case GREEN:   this.red=false;
                          this.green=true;
                          this.blue=false;
                          break;
            case BLUE:    this.red=false;
                          this.green=false;
                          this.blue=true;
                          break;
            case YELLOW:  this.red=true;
                          this.green=true;
                          this.blue=false;
                          break;
            case CYAN:    this.red=false;
                          this.green=true;
                          this.blue=true;
                          break;
            case MAGENTA: this.red=true;
                          this.green=false;
                          this.blue=true;
                          break;
            case WHITE:   this.red=true;
                          this.green=true;
                          this.blue=true;
                          break;
        }
    }

    public Constants.PHASE getPhaseConstant(){
        if(this.red){
            if(this.blue){
                if(this.green){
                    return Constants.PHASE.WHITE;
                }
                else return Constants.PHASE.MAGENTA;
            }
            else if(this.green){
                return Constants.PHASE.YELLOW;
            }
            else return Constants.PHASE.RED;
        }
        else if(this.blue){
            if(this.green){
                return Constants.PHASE.CYAN;
            }
            else return Constants.PHASE.BLUE;
        }
        else if(this.green){
            return Constants.PHASE.GREEN;
        }
        else return Constants.PHASE.BLUE;
    }

    public int activeColours(){
        int colours = 0;
        if(red) colours++;
        if(green) colours++;
        if(blue) colours++;
        return colours;
    }

    public boolean interactable(Phase phase, boolean world){
        Constants.PHASE phaseConst = this.getPhaseConstant();
        Constants.PHASE otherPhaseConst = phase.getPhaseConstant();
        if(otherPhaseConst== Constants.PHASE.WHITE){
            return true;
        }
        switch(phaseConst){
            case RED:
                if(otherPhaseConst== Constants.PHASE.RED||otherPhaseConst== Constants.PHASE.YELLOW||otherPhaseConst== Constants.PHASE.MAGENTA) {
                    return true;
                }
                break;
            case GREEN:
                if(otherPhaseConst== Constants.PHASE.GREEN||otherPhaseConst== Constants.PHASE.YELLOW||otherPhaseConst== Constants.PHASE.CYAN) {
                    return true;
                }
                break;
            case BLUE:
                if(otherPhaseConst== Constants.PHASE.BLUE||otherPhaseConst== Constants.PHASE.CYAN||otherPhaseConst== Constants.PHASE.MAGENTA) {
                    return true;
                }
                break;
            case YELLOW:
                if(otherPhaseConst== Constants.PHASE.YELLOW) {
                    return true;
                }
                else if(!world&&(otherPhaseConst== Constants.PHASE.RED||otherPhaseConst== Constants.PHASE.GREEN)){
                    return true;
                }
                break;
            case CYAN:
                if(otherPhaseConst== Constants.PHASE.CYAN) {
                    return true;
                }
                else if(!world&&(otherPhaseConst== Constants.PHASE.BLUE||otherPhaseConst== Constants.PHASE.GREEN)){
                    return true;
                }
                break;
            case MAGENTA:
                if(otherPhaseConst== Constants.PHASE.MAGENTA) {
                    return true;
                }
                else if(!world&&(otherPhaseConst== Constants.PHASE.BLUE||otherPhaseConst== Constants.PHASE.RED)){
                    return true;
                }
                break;
            case WHITE:
                return true;
        }
        return false;
    }

    public boolean interactable(Phase phase,Phase worldPhase){
            if(this.interactable(worldPhase, true)&&(phase.interactable(worldPhase, true))){
                return true;
            }
            else return false;
    }

    public Color getTint(){
        float r,g,b;
        if(red){r=1;}else{r=0;};
        if(green){g=1;}else{g=0;};
        if(blue){b=1;}else{b=0;};
        return new Color(r,g,b,1);
    }

}
