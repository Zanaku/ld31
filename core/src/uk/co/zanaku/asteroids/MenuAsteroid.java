package uk.co.zanaku.asteroids;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by Tom on 07/12/2014.
 */
public class MenuAsteroid implements Phased{

    public Asteroids game;

    public Phase phase;

    public BodyDef bodyDef;
    public Body body;
    public CircleShape shape;
    public FixtureDef fixtureDef;
    public Fixture fixture;

    public Texture texture;
    public Sprite sprite;

    public int task;
    public static int QUIT = 0;
    public static int START_SCORE_ATTACK = 1;
    public static int RESOLUTION_1024x576 = 2;
    public static int RESOLUTION_1280x720 = 3;
    public static int RESOLUTION_1920x1080 = 4;
    public static int FULLSCREEN = 5;

    public MenuAsteroid(World world, AssetManager assets, String textureName,int task ,Asteroids game,Phase phase, float x, float y){
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x,y);

        body = world.createBody(bodyDef);
        shape = new CircleShape();

        fixtureDef = new FixtureDef();
        fixtureDef.restitution = 1f;
        if(task<2){
            shape.setRadius(10);
        }
        else{
            shape.setRadius(8f);
        }
        fixtureDef.density = 1f;
        texture = assets.get(textureName);
        texture.setFilter(Constants.filter, Constants.filter);

        fixtureDef.shape = shape;
        fixture = body.createFixture(fixtureDef);

        sprite = new Sprite(texture);
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);

        body.setUserData(this);

        this.phase = phase;

        this.task = task;

        this.game = game;
    }

    public void redraw(){
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);
    }

    public void render(SpriteBatch batch,Phase worldPhase){
        Color tint = this.phase.getTint();
        if(!phase.interactable(worldPhase, true)) {
            tint.a=0.2f;
        }
        sprite.setColor(tint);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setPosition(body.getPosition().x*Constants.BOX_TO_WORLD-sprite.getWidth()/2,body.getPosition().y*Constants.BOX_TO_WORLD-sprite.getHeight()/2);
        sprite.draw(batch);
    }

    public void doTask(){
        if(task==QUIT){
            Gdx.app.exit();
        }
        else if(task==START_SCORE_ATTACK){
            game.changeMode(1);
        }
        else if(task== RESOLUTION_1024x576){
            game.resizeWindow(1024,576,Gdx.graphics.isFullscreen());
        }
        else if(task==RESOLUTION_1280x720){
            game.resizeWindow(1280, 720, Gdx.graphics.isFullscreen());
        }
        else if(task==RESOLUTION_1920x1080){
            game.resizeWindow(1920, 1080, Gdx.graphics.isFullscreen());
        }
        else if(task==FULLSCREEN){
            if(Gdx.graphics.isFullscreen())
                Gdx.graphics.setDisplayMode(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),false);
            else
                Gdx.graphics.setDisplayMode(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),true);
        }
    }

    public void dispose(){
        shape.dispose();
    }

    @Override
    public Phase getPhase() {
        return phase;
    }

    @Override
    public void setPhase(Phase phase) {
        this.phase = phase;
    }
}
