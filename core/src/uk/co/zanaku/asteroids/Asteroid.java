package uk.co.zanaku.asteroids;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Tom on 06/12/2014.
 */
public class Asteroid implements Phased{

    public int type;

    public static int SMALL = 0;
    public static int MEDIUM = 1;
    public static int LARGE = 2;

    public BodyDef bodyDef;
    public Body body;
    public CircleShape shape;
    public FixtureDef fixtureDef;
    public Fixture fixture;

    public Vector2 linearVelocityAtTimeOfDeath;

    public Texture texture;
    public Sprite sprite;

    public Phase phase;


    public Asteroid(World world, AssetManager assets, Phase phase, int type, float x, float y){
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x,y);

        body = world.createBody(bodyDef);
        shape = new CircleShape();

        fixtureDef = new FixtureDef();
        fixtureDef.restitution = 1f;

        this.type = type;

        if(this.type==SMALL){
            shape.setRadius(1.6f);
            fixtureDef.density = 1.6f;
            texture = assets.get("asteroidSmall.png");
        }
        else if(this.type==MEDIUM){
            shape.setRadius(5);
            fixtureDef.density = 5f;
            texture = assets.get("asteroidMedium.png");
        }
        else if(this.type==LARGE){
            shape.setRadius(10);
            fixtureDef.density = 10f;
            texture = assets.get("asteroidLarge.png");
        }

        texture.setFilter(Constants.filter,Constants.filter);
        fixtureDef.shape = shape;
        fixture = body.createFixture(fixtureDef);

        sprite = new Sprite(texture);
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);

        body.setUserData(this);

        this.phase = phase;
    }

    public void shatter(World world, AssetManager assets, Array<Asteroid> asteroids){
        float radians, dx, dy;

        if(this.type==LARGE){
            for(int i=0;i<2;i++){
                radians = MathUtils.radiansToDegrees*MathUtils.random(0,360);
                dx = MathUtils.cos(radians) * 6f;
                dy = MathUtils.sin(radians) * 6f;
                Asteroid asteroid1 = new Asteroid(world, assets,new Phase(this.phase), MEDIUM, this.body.getPosition().x,this.body.getPosition().y);
                asteroid1.body.setLinearVelocity(new Vector2(dx,dy));
                asteroids.add(asteroid1);
            }
        }
        if(this.type==MEDIUM){
            for(int i=0;i<3;i++){
                radians = MathUtils.radiansToDegrees*MathUtils.random(0,360);
                dx = MathUtils.cos(radians) * 8f;
                dy = MathUtils.sin(radians) * 8f;
                Asteroid asteroid1 = new Asteroid(world, assets,new Phase(this.phase), SMALL, this.body.getPosition().x,this.body.getPosition().y);
                asteroid1.body.setLinearVelocity(new Vector2(dx,dy));
                asteroids.add(asteroid1);
            }

        }

    }

    public void redraw(){
        sprite.setSize(shape.getRadius()*2*Constants.BOX_TO_WORLD,shape.getRadius()*2*Constants.BOX_TO_WORLD);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setOriginCenter();
        sprite.setPosition(body.getPosition().x,body.getPosition().y);
    }

    public void render(SpriteBatch batch,Phase worldPhase){
        Color tint = this.phase.getTint();
        if(!phase.interactable(worldPhase, true)) {
            tint.a=0.2f;
        }
        sprite.setColor(tint);
        sprite.setRotation(MathUtils.radiansToDegrees*body.getAngle());
        sprite.setPosition(body.getPosition().x*Constants.BOX_TO_WORLD-sprite.getWidth()/2,body.getPosition().y*Constants.BOX_TO_WORLD-sprite.getHeight()/2);
        sprite.draw(batch);
    }

    public void dispose(){
        shape.dispose();
    }

    @Override
    public Phase getPhase() {
        return phase;
    }

    @Override
    public void setPhase(Phase phase) {
        this.phase = phase;
    }

}
