package uk.co.zanaku.asteroids.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import uk.co.zanaku.asteroids.Asteroids;
import uk.co.zanaku.asteroids.Constants;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1280;
        config.height = 720;
        config.fullscreen= false;
        config.vSyncEnabled= false;
        config.title = "Ludumoids";
        config.addIcon("appIcon128.png", Files.FileType.Local);
        config.addIcon("appIcon32.png", Files.FileType.Local);
        config.addIcon("appIcon16.png", Files.FileType.Local);
        new LwjglApplication(new Asteroids(), config);
	}
}
